# googleadmob Service
===================================

![googleadmob-popover](images/popover.png)

### Description

Google Admob is the largest advertising platform integration into own applications. It includes this service to start hosting advertising pieces for advertisers interested in reaching your audience and receive income from the first click. Monetize your app, takes advantage of its potential to generate income and give an economic boost to your company.


### Images
- [Logo](images/logo.png)
- [Screenshot](images/screenshot01.png)


### Details:

- Author:
- Version: 0.0.2
- Homepage:
