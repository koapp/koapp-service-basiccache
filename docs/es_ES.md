# Documentación

### Deep links

Este servicio le permite administrar qué enlaces profundos se pueden usar en su aplicación.

Para usar este servicio, solo active o desactive la casilla de verificación en las aplicaciones que va a vincular desde su aplicación.

Si el enlace de la aplicación que necesita no está en la lista, haga clic en el botón "Add Custom deep link".
En el campo "Deep link name:" inserte un nombre descriptivo de su enlace profundo personalizado.
En el campo "Deep link code:" inserte la palabra clave de su enlace profundo.

Ejemplo:

Facebook
Deep link name: Facebook
Deep link code: fb

Para obtener más información sobre cómo usar enlaces profundos, consulte esto