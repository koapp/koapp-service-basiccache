# Documentation

### Deep links

This service allows you to manage what deep links can be used in your app.

To use this service just enable or disable the checkbox in the apps you gonna link from your app.

If the app you need link is not in the list just click the "Add Custom deep link" button.
In the field "Deep link name:" insert a descritive name of your custom deep link.
In the field "Deep link code:" insert the keyword of your deep link.

Example:

Facebook
Deep link name: Facebook
Deep link code: fb

For more info about how to use deep links check this [Guide to Deep Linking](https://support.visiolink.com/hc/en-us/articles/212807305-Guide-to-Deep-Linking)