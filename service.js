(function () {
  angular
    .module('king.services.basiccache', [])
    .run(basiccache);

  basiccache.$inject = ['configService'];

  function basiccache(configService) {
    // Register upper level modules
    if (configService.services && configService.services.basiccache) {
      console.log("basiccache service enabled");
      console.log("enabled links:");
      var config = configService.services.basiccache.scope;
      
      if(!window || !window.requestFileSystem) return;

      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {

        //var absPath = "file:///storage/emulated/0/";
        var absPath = cordova.file.externalRootDirectory;
        var fileDir = cordova.file.externalDataDirectory.replace(cordova.file.externalRootDirectory, '');
        var fileName = "somename.txt";
        var filePath = fileDir + fileName;

        fs.root.getFile(filePath, { create: true, exclusive: false }, function (fileEntry) {
          //writeFile(fileEntry, BINARY_ARR).then(function () {
            //do something here
          //});
        }, function (err) { });
      }, function (err) { });

      function writeFile(fileEntry, dataObj) {
        return $q(function (resolve, reject) {
          fileEntry.createWriter(function (fileWriter) {
            fileWriter.onwriteend = function () {
              resolve();
            };
            fileWriter.onerror = function (e) {
              reject(e);
            };
            fileWriter.write(dataObj);
          });
        });
      }
    }

    // --- End servicenameController content ---
  }
})();
